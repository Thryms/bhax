#include <stdio.h>

int main()
{

   int a = 3;
   int b = 12;

   printf("a=");
   printf("%d\n", a);
   printf("b=");
   printf("%d\n", b);

   b = a+b;
   a = b-a;
   b = b-a;

   printf("a=");
   printf("%d\n", a);
   printf("b=");
   printf("%d\n", b);
}

